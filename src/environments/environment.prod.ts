export const environment = {
  production: true,
  restBaseUrl: 'https://bah-mts-api.eaiapps.com',
  oktaAdminUrl: 'https://ssodev.mts.bah.com/home/admin-entry',
  rotatingCoinInterval: 2500
};
