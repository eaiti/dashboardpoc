import { Component, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-rotating-coin',
  templateUrl: './rotating-coin.component.html',
  styleUrls: ['./rotating-coin.component.scss'],
})
export class RotatingCoinComponent {
  coinSource: string;
  coinTimer: Subscription;
  sourceItr = 0;

  coinInfo = [
    {
      imgSrc: '../assets/slide1.png',
      title: 'Our Challenge Coin',
      description: 'This coin tells the story of our team and how we work with you.'
    },
    {
      imgSrc: '../assets/slide2.png',
      title: 'The Archer',
      description: 'The archer represents the importance of having an expert use a precision weapon.'
    },
    {
      imgSrc: '../assets/slide3.png',
      title: 'Excellence Through Effort',
      description: 'We do not automate away threat detection and response.'
    },
    {
      imgSrc: '../assets/slide4.png',
      title: 'October 31, 2017 at 2:19:42PM',
      description: 'Hexadecimally encoded in the coin’s serial number is a representation of the EPOCH date that MTS was founded.'
    }
  ];

  constructor() {
    this.coinTimer = interval(environment.rotatingCoinInterval).subscribe((x) => {
      this.rotateCoin();
    });
  }

  rotateCoin(): void {
    this.sourceItr++;
    if (this.sourceItr >= this.coinInfo.length) {
      this.sourceItr = 0;
    }
  }
}