import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RotatingCoinComponent } from './rotating-coin.component';

describe('RotatingCoinComponent', () => {
  let component: RotatingCoinComponent;
  let fixture: ComponentFixture<RotatingCoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RotatingCoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RotatingCoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
