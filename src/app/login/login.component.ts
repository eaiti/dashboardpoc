import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { OktaService } from '../services/okta.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private oktaService: OktaService) { }

  ngOnInit(): void {
    this.oktaService.login();
  }

}
