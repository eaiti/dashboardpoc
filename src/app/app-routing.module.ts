import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RotatingMockupComponent } from './rotating-mockup/rotating-mockup.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationGuard } from './authentication.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'rotating', component: RotatingMockupComponent, canActivate: [AuthenticationGuard] },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
