import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { App } from '../models/app';
import { Role } from '../models/role';
import { tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root',
})
export class OktaService {
  currentUser: User;
  constructor(private http: HttpClient) {}

  getOktaApplications(): Observable<App[]> {
    return this.http.get<App[]>(`${environment.restBaseUrl}/application`, httpOptions);
  }

  me(): Observable<User> {
      return this.http.get<User>(`${environment.restBaseUrl}/auth/me`, httpOptions)
          .pipe(
            tap(user => this.currentUser = user)
          );
  }

  getUserRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(`${environment.restBaseUrl}/auth/roles`, httpOptions);
  }

  login(): void {
    window.location.href = `${environment.restBaseUrl}/login`;
  }
}
