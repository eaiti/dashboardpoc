export interface User {
    userId: string;
    firstName: string;
    lastName: string;
    username: string;
    initials: string;
    loginDate: Date;
}