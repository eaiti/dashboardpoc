export interface Role {
    id: string;
    label: string;
    status: string;
    type: string;
    assignmentType: string;
}
