export interface App {
  _links: {
    activate?: {
      href: string;
    };
    appLinks?: [
      {
        href: string;
        name: string;
        type: string;
      }
    ];
    logo?: [
      {
        href: string;
        name: string;
        type: string;
      }
    ];
    groups?: { href: string }
    help?: { href: string; type: string}
    metadata?: { href: string; type: string; }
    users?: { href: string; }
    deactivate?: object
  };
  accessibility: object;
  created: string;
  credentials: object;
  features: any;
  id: string;
  label: string;
  lastUpdated: string;
  name: string;
  settings: object;
  signOnMode: string;
  status: string;
  visibility: object;
}
