import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RotatingMockupComponent } from './rotating-mockup.component';

describe('RotatingMockupComponent', () => {
  let component: RotatingMockupComponent;
  let fixture: ComponentFixture<RotatingMockupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RotatingMockupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RotatingMockupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
