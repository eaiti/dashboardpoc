import { Component, OnInit } from '@angular/core';
import { OktaService } from '../services/okta.service';
import { App } from '../models/app';
import { User } from '../models/user';
import { Role } from '../models/role';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rotating-mockup',
  templateUrl: './rotating-mockup.component.html',
  styleUrls: ['./rotating-mockup.component.scss']
})
export class RotatingMockupComponent implements OnInit {
  apps: App[] = [];
  currentUser: User;
  userRoles: Role[];

  constructor(private oktaService: OktaService, private router: Router) {
    if (this.oktaService.currentUser) {
      this.currentUser = this.oktaService.currentUser;
      this.currentUser.initials = this.currentUser?.firstName?.charAt(0).toUpperCase() + this.currentUser.lastName?.charAt(0).toUpperCase();
    }
  }

  ngOnInit(): void {
    this.oktaService.getOktaApplications().subscribe(
      (response: App[]) => {
        for (const app of response) {
          if (app.label !== 'Dashboard Mock') {
            this.apps.push(app);
          }
        }
      },
      (response) => {
        response.error.errors.forEach((e) => {
          throw e.errorMessage;
        });
      }
    );

    this.oktaService.getUserRoles().subscribe(
      (response: Role[]) => {
        this.userRoles = response;
      },
      (response) => {
        response.error.errors.forEach((e) => {
          throw e.errorMessage;
        });
      }
    );
  }

  isAdmin(): boolean {
    return this.userRoles?.map(role => role.type).filter(roleType => roleType.includes('ADMIN')).length > 0;
  }

  logout() {
    this.oktaService.me().subscribe(res => {
      const form = document.getElementById('logout') as HTMLFormElement;
      form.method = 'POST';
      form.action = `${environment.restBaseUrl}/logout`;
      form.submit();
    },
    err => {
      this.router.navigateByUrl('/login');
    });
  }

  goToOktaAdmin() {
    window.open(environment.oktaAdminUrl, '_blank');
  }
}
